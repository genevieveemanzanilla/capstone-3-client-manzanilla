const express = require('express');
const router = express.Router();
const userController = require('./../controllers/user');
const auth = require('./../auth');

router.post('/', (req, res) => {
	userController.register(req.body).then(result => res.send(result))
})

router.post('/email-exists', (req, res) => {
	userController.emailExists(req.body).then(result => res.send(result))
})

router.post('/login', (req,res) =>{
	// userController.login(req.body).then(result => res.send(result))
	userController.login(req,res)
})
router.post('/record', auth.verify,(req,res) => {
	// userId = get this in token
	// category = body
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		category: req.body.category,
		description: req.body.category,
		amount: req.body.amount,
		date: req.body.date,
		type: req.body.type
	}

	userController.record(params).then( result => res.send(result))
})

router.get('/', (req,res) => {

	userController.getAllUsers().then(result => res.send(result))	

})

router.get('/details', auth.verify, (req,res) => {
	const decodedToken = auth.decode(req.headers.authorization)
	userController.get({ userId : decodedToken.id}).then( user => res.send( user))
})

router.post('/verify-google-id-token', async (req,res) => {
	res.send(await userController.verifyGoogleTokenId(req.body.tokenId,req.body.accessToken))
})

router.post('/category', auth.verify,(req,res)=>{

	userController.addCategory(req.body).then(result => res.send(result))
})

//add Transaction
router.post('/transaction', auth.verify,(req,res)=>{
	userController.addTransaction(req.body).then(result => res.send(result))
})

//get all transactions
router.get('/allTransactions',  (req,res)=>{
const decodedToken = auth.decode(req.headers.authorization)
userController.getAllTransactions({userId : decodedToken.id}).then(user => res.send(user))


})

//get all Categories
router.get('/allCategories',  (req,res)=>{
const decodedToken = auth.decode(req.headers.authorization)
userController.getAllCategories({userId : decodedToken.id}).then(user => res.send(user))
})

//get balance
router.get('/balance', (req,res)=>{
const decodedToken = auth.decode(req.headers.authorization)
userController.getAllBalance({userId : decodedToken.id}).then(user => res.send(user))
})

//Update Balance
router.put('/updateBalance', auth.verify,(req,res)=>{

	userController.updateBalance(req.body).then(result => res.send(result))
})

// find transaction by description
router.get('/search',(req,res)=>{
	const decodedToken = auth.decode(req.headers.authorization)
	userController.searchTransaction({userId: decodedToken.id}, req.body).then(result => res.send(result))
})


module.exports = router;

/*function Hello(message){
	const promise = new Promise
}

Hello("message");

fetch('http://localhost:8000/api/users/email-exists')
.then(res => res.json())*/