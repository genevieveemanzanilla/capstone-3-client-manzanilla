const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const nodemailer = require('nodemailer')

const port = process.env.PORT || 8000;
const app = express();
require('dotenv').config();

// database connection
mongoose.connect(process.env.DB, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false	
});

// check connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log(`Connected to database`);
});

/*const corsOptions = {
	origin: ['http://localhost:3000'],
	optionsSuccessStatus: 200
}*/

app.use(cors());
app.options('*', cors())

// allow resource sharing from all origins
// app.use(cors());

// middlewares
app.use(express.json());

// routes

const userRoutes = require('./routes/user');
app.use('/api/users', userRoutes)

// email sending test

// nodemailer sender details
/*
const transporter = nodemailer.createTransport({

	/*
	
	SMTP - Simple Mail Transfer Protocol - communication protocol for email transmission.
	host - host name
	auth - defines authentication data
	port - the port to connect to. The default is 587. If the port is unsecured, the port number is 587 but ftom a secured host, it is 465.

	

	host: 'smtp.gmail.com',
	port: 465,
	auth: {

        user: 'mangenela31@gmail.com',
        pass: '15321532'

	}
})

// nodemailer mail details
transporter.sendMail({

	from: 'test@example.com',
	//ethereal eamil
	to: 'wallace87@ethereal.email',
	subject: 'Thanks for registering to Next Booking!',
	// Message: text and html
	// text is uses if the email does not accept html format
	text: 'Hi my name is Gen, we at zuitt developed this next booking system to integrate email sending feature in next.js and react elements. Thank you for registering.',
	html: '<p>Hi my name is Gen, we at zuitt developed this next booking system to integrate email sending feature in next.js and react elements. Thank you for registering.</p>'


})
*/

app.listen(port , () => {
	console.log(`App is listening on port ${port}`);
});