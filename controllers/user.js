const User = require('../models/user');
const bcrypt = require('bcrypt');
const { createAccessToken } = require('./../auth');
const nodemailer = require('nodemailer')

const {OAuth2Client} = require('google-auth-library')

const clientId="348746014898-nd78bldlub1cfcatpi0bp97rrm0h5jj6.apps.googleusercontent.com"

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		password: bcrypt.hashSync(params.password, 10),
		loginType: "email",
		categories: params.categories
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
	})
}

module.exports.login = (req,res) => {
	User.findOne({email : req.body.email})
	.then( user => {
		if( !user ) { 
			res.send(false)
		} else {
			// res.send(user)
			let comparePasswordResult = bcrypt.compareSync(req.body.password, user.password); 
			if (!comparePasswordResult) {
				res.send(false)
			} else {
				
				res.send({accessToken : createAccessToken(user)})
			}
		}
	}).catch( err => {
		res.status(500).send("Server Error")
	})

}

module.exports.get = (params) => {
	return User.findById(params.userId).select({ password: 0}).then( user => {
		return user
	})
}




module.exports.getAllUsers = () => {


	return User.find({}).then(resultFromFind => resultFromFind)


}


module.exports.verifyGoogleTokenId = async (tokenId,googleAccessToken) => {

	console.log(googleAccessToken)

	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})

	

	if(data.payload.email_verified === true){

		const user = await User.findOne({email: data.payload.email}).exec()

		if(user !== null){
			console.log("A user with the same email has been registered.")
			console.log(user)

			if(user.loginType === "google"){

				return {accessToken: createAccessToken(user)}
				//function

			} else {

				return {error: 'login-type-error'}
			}


		} else {

			console.log(data.payload)
			const newUser = new User({

				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: "google",
				categories: params.categories

			})

			return newUser.save().then((user,err)=>{

				const mailOptions = {

					from: 'genevieveemanzanilla@gmail.com',
					to:user.email,
					subject: 'Pocket Gem Registration',
					text: `You registered to Pocket Gem on ${new Date().toLocaleString()}`,
					html: `You registered to Pocket Gem on ${new Date().toLocaleString()}`
				}

				const transporter = nodemailer.createTransport({

					host: 'smtp.gmail.com',
					port: 465,
					auth: {

						type:'OAuth2',
						user: process.env.MAILING_SERVICE_EMAIL,
						clientId: process.env.MAILING_SERVICE_CLIENT_ID,
						clientSecret: process.env.MAILING_SERVICE_CLIENT_SECRET,
						refreshToken: process.env.MAILING_SERVICE_REFRESH_TOKEN,
						accessToken: googleAccessToken

					}
				})

				function sendMail(transporter){

					transporter.sendMail(mailOptions, function(err,result){

						if(err){
							console.log(err)
							transporter.close()

						} else if (result) {
							console.log(result)
							transporter.close()
						}
					})
				}

				sendMail(transporter)

				return {accessToken:createAccessToken(user)}
			})
		}

	} else {

		return { error:"google-auth-error"}
	}

}

// module.exports.record = params => {

// 	return User.findById(params.userId).then( user => {
// 		user.records.push({category: params.category,
// 		description: params.category,
// 		amount: params.amount,
// 		date: params.date,
// 		type: params.type})
// 		return user.save().then((user) => {
// 		})
// 	})
// }

//Add category
module.exports.addCategory = (params) =>{
	return User.findById(params.id).then(category =>{
		category.categories.push({name: params.name, type: params.type})
		return category.save().then(result =>{
			return result ? true : false
		})
	})
}

//Add Transaction
module.exports.addTransaction = (params) =>{

	const  {id, type,category, amount, description, balance} = params
	return User.findById(id).then(transaction=>{

		transaction.records.push({type,category, amount, description, balance})
				return transaction.save().then(result =>{
					return result ? true : false
				})
	})
}

//Get ALL transactions
module.exports.getAllTransactions = (params) =>{

	//main return
	return User.findById(params.userId).then(transaction =>{
		return transaction.records
	})
}

//Get all Transactions
// To get the categories, manipulate the array in the FE
module.exports.getAllCategories = (params) =>{

	//main return
	return User.findById(params.userId).then(category =>{
		return category.categories
	})
}

//Get Balance
module.exports.getAllBalance = (params) =>{

	//main return
	return User.findById(params.userId).then(result =>{
		return result.balance
	})
}


//Update Balance
module.exports.updateBalance = (params) =>{
	console.log(params)

	 return User.findByIdAndUpdate(params.id, {balance: params.balance})
	.then( (doc, err )=> {
	 	return err ? false : true
    })
}

//Find transaction by description getAllTransations => .find()
module.exports.getAllTransactions = (params) =>{

	//main return
	return User.findById(params.userId).then(transaction =>{
		return transaction.records
	})
}


//search
module.exports.searchTransaction = (params) =>{


	console.log(params)

	//main return
	 return User.findById(params.userId).then(user =>{
	 	return user.records.find(transaction=>{
	 		if(transaction.description === params.description){
	 			return transaction
			}else{
				return false
			}
		})
	})
}