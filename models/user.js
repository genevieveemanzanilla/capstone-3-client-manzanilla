const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name is required.']
    },
    lastName: {
        type: String,
        required: [true, 'Last name is required.']
    },
    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    password: {
        type: String,
    },
    loginType:{
        type: String,
        required: [true, 'Login type is required']
    },
    balance: {
        type: String,
        required: [true, "Balance is required"],
        default: 0
    },
    categories: [
        {
            name: {
                type: String,
                required: [true, 'Category name is required']
            },
            type: {
                type: String
                // Values are 'Income' and 'Expenses'.
            }
        }
    ],
    records: [
        {
            description: {
                type: String,
                required: [true, 'category is required']
            },
            amount: {
                type: String,
            },
            date: {
                type: Date,
                default: new Date()
            },
            category:{
                type: String
            },
            type: {
                type: String,
                // Values are 'Income' and 'Expenses'.
            },
            balance: {
                type: String,
            }
        }
    ]
})

module.exports = mongoose.model('User', userSchema)